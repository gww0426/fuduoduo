//index.js
//获取应用实例
const app = getApp();
var mta = require('../../utils/mta_analysis.js');
Page({
  data: {
    banner: '/resources/banner.png',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    activitys:[],
    statusConvertor:{'0':'进行中', '-1':'已结束'}
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.initData(options);
  },
  onLoad: function (options) {
    mta.Page.init();
    
  },
  onShow: function(options){
    this.initData(options);
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  initData: function(options){
    this.getActivitys(options);
  },
  getActivitys: function (options) {

    var that = this;

    wx.showLoading({
      title: '数据加载中',
    })

    wx.request({
      url: app.host + '/card/list',
      data: { t: new Date().getTime() },
      method: 'GET',
      header: {
        "Content-Type": "applciation/json"
      },
      success: function (res) {
        if (res && res.data && res.data.code > 0) {
          wx.hideLoading();
          var activitys = res.data.data.activitys;
          that.setData({ activitys: activitys });
        } else {
          wx.showToast({
            title: '活动列表加载失败,请售后再试',
          })

        }
      },
      fail: function () {
        wx.hideLoading();
      }
    })
  }
})
