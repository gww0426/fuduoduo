import initAreaPicker, { getSelectedAreaData } from '../../template/citySelector/index';
const util = require('../../utils/util.js');
var mta = require('../../utils/mta_analysis.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    customItem: '全部',
    address:{
      id:0,
      phone:'',
      contact:'',
      province:'',
      city:'',
      area:'',
      street:''      
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.address.id = options.id;
    mta.Page.init();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    this.initData(options);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  initData: function(options){
    let id = this.data.address.id || options.id;
    if(id > 0){
      this.getAddressDetail(id);
    }
  },
  getAddressDetail: function(id){
    let that = this;
    wx.showLoading({
      title: '数据加载中',
    })
    wx.request({
      url: app.host + '/user/address/detail',
      data: {id: id},
      method: 'GET',
      success: function (res) {
        if (res && res.data && res.data.code > 0) {
          wx.hideLoading();
          let address = res.data.data.address;
          that.setData({ address: address});

          let addressInfo = address.province + '-' + address.city + '-' + address.area;
          if (address && !address.area){
            addressInfo = address.province + '-' + address.city;
          }
          that.setData({ areaPicker: { address: addressInfo }});
        } else {
          wx.showToast({
            title: '地址获取失败,请售后再试',
          })

        }
      },
      fail: function () {
        wx.hideLoading();
      }
    })
  },
  bindClickSelectArea: function(){
    let that = this;
    initAreaPicker({
      'ok':function(){
        console.log('cityselector ok');//完成了省市区选择
        that.data.address.province = that.data.areaPicker.selected[0].name;
        that.data.address.city = that.data.areaPicker.selected[1].name;
        if (that.data.areaPicker.selected[2]){
          that.data.address.area = that.data.areaPicker.selected[2].name;
        }
      }
    });
  },
  bindClickCancel: function(){
    wx.navigateBack({});
  },
  bindContactChange: function(e){
    this.data.address.contact = e.detail.value;
  },
  bindPhoneChange: function (e) {
    this.data.address.phone = e.detail.value;
  },
  bindStreetChange: function (e) {
    this.data.address.street = e.detail.value;
  },
  bindSubmit: function(e){

    let that = this;
    let address = that.data.address;

    console.log(address);

    let data = address;
    data.formId = e.detail.formId;
    let openid = app.globalData.openid || wx.getStorageSync('openid');
    data.openid = openid;

    if (data.id == undefined){
      data.id = 0;
    }

    wx.request({
      url: app.host + '/user/address/save',
      data: data,
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        if (res && res.data && res.data.code > 0) {
          wx.hideLoading();
          wx.showToast({
            title: '地址添加成功',
          });
          wx.navigateBack({});
        } else {
          wx.showToast({
            title: '地址添加失败,请售后再试',
          })

        }
      },
      fail: function () {
        wx.hideLoading();
      }
    })

  },
  bindDeleteAddress: function(){
    let that = this;
    let addressId = that.data.address.id;
    wx.showModal({
      title: '删除地址',
      content: '即将删除地址，确定要删除吗？',
      success: function (res) {
        if (res.confirm) {
          wx.request({
            url: app.host + '/user/address/delete',
            data: { id: addressId },
            method: 'GET',
            header: {
              "Content-Type": "applciation/json"
            },
            success: function (res) {
              if (res && res.data && res.data.code > 0) {
                wx.hideLoading();
                wx.showToast({
                  title: '地址删除成功',
                });
                wx.navigateBack({});
              } else {
                wx.showToast({
                  title: '地址删除失败,请售后再试',
                })

              }
            },
            fail: function () {
              wx.hideLoading();
            }
          })
        }
      }
    })
  },
  bindClickWxAddress: function(){
    let that = this;
    wx.chooseAddress({
      success: function (res) {
        let address = {};
        address.phone = res.telNumber;
        address.contact = res.userName;
        address.province = res.provinceName;
        address.city = res.cityName;
        address.area = res.countyName;
        address.street = res.detailInfo;
        that.setData({address: address});

        let addressInfo = address.province + '-' + address.city + '-' + address.area;
        if (address && !address.area) {
          addressInfo = address.province + '-' + address.city;
        }
        that.setData({ areaPicker: { address: addressInfo } });
      }
    })
  }
})