const wxLogin = () => {
  wx.getSetting({
    success(res) {
      console.log('xxxxxxxxxxxxxxxx');
      console.log(res);
      if (!res.authSetting['scope.userInfo']) {
        console.log('用户未授权登录，发起授权');
        wx.authorize({
          scope: 'scope.userInfo',
          success() {
            //用户同意微信登录,则微信登录
            console.log('用户同意微信登录')
          },
          complete(res){
            console.log(res);
          }
        })
      }else{
        console.log('用户已授权登录')
      }
    }
  })
}

module.exports = {
  wxLogin: wxLogin
}